﻿using System;
using System.Text;
using System.Collections.Generic;

namespace NiceNumbers
{

    /// <summary>
    ///  Генерирует красивые числа по шаблону
    ///  {левая часть} {центральная цифра} {правая часть}
    ///  Алгоритм генерации: 
    ///  1. По всем значениям X возможной суммы цифр от 0 до 72
    ///  2. отобрать множество всех 13-ричных 6-значных чисел, где сумма чисел равна  X
    ///  3. сгеренировать декартовым произведением комбинации {левая часть} {центральная цифра} {правая часть}, где 
    ///     {левая часть} и {правая часть} лежат внутри отобранного множества
    ///     центральная цифра менятеся от 0 до С (12)
    /// </summary>
    public class NiceNumberPrinter
    {
        Statistic _statistic;
        public NiceNumberPrinter(Statistic statistic)
        {
            _statistic = statistic ?? throw new ArgumentNullException(nameof(statistic));
        }

        public IEnumerable<string> GetNiceNumbers()
        {
            // Прохожу по всем классам. Внутри класса сумма цифр одинакова.

            for (int i = 0; i < _statistic.Count(); i++)
            {
                foreach (string niceNumber in PrintNiceNumbersForClassWithSum(i))
                {
                    yield return niceNumber;
                }
            }
        }


        private IEnumerable<string> PrintNiceNumbersForClassWithSum(int digitsSum)
        {
            // Генерирую красивые числа для класса с суммой цифр = digitsSum.
            // Сразу для всех центральных чисел
            // Отбираю все числа, которые имеют сумму цифр равную digitsSum в левой части
            // Отбираю все числа, которые имеют сумму цифр равную digitsSum в правой части
            // Генерирую числа вида {левая часть} {центральная цифра} {правая часть}
            var numbersBuilder = _statistic.GetNumberBuilder();
            foreach (Int32 leftPart in numbersBuilder.ItemsWithSum(digitsSum))
            {
                var leftPartWithLeadingZeroes = PrintNumberWithLeadingZeroes(leftPart);

                foreach (Int32 rightPart in numbersBuilder.ItemsWithSum(digitsSum))
                {
                    var rightPartWithLeadingZeroes = PrintNumberWithLeadingZeroes(rightPart);

                    for (Int32 centralDigit = 0; centralDigit < numbersBuilder.DigitBase; centralDigit++)
                    {
                        char centralNumber = NumberUtil.PrintDigit(centralDigit);
                        string niceNumber = $"{leftPartWithLeadingZeroes}{centralNumber}{rightPartWithLeadingZeroes}";
                        
                        yield return niceNumber;
                    }
                }
            }

        }

        string PrintNumberWithLeadingZeroes(Int32 number)
        {
            NumberUtil.PrintNumber(sbNumber, number, _statistic.GetNumberBuilder().DigitBase, _statistic.GetNumberBuilder().Places);
            return sbNumber.ToString();
        }

        StringBuilder sbNumber = new StringBuilder(128);
    }

}
