﻿using System;

namespace NiceNumbers
{
    /// <summary>
    /// Класс вычисляет количество красивых чисел по условиям задачи
    /// Также выводит первые printCount таких чисел
    /// </summary>
    public class NiceApplication
    {
        NumbersBuilder _numbersBuilder;
        Statistic _statistic;

        Int32 PrintCount;

        /// <summary>
        /// Создает приложение, выполняющее работу
        /// </summary>
        /// <param name="digitBase">Система счисления</param>
        /// <param name="positionsInNiceNumber">Кол-во позиций в красивом числе</param>
        /// <param name="printCount">Кол-во первых красивых чисел, которые выводятся на печать</param>
        public NiceApplication(Int32 digitBase, Int32 positionsInNiceNumber, Int32 printCount)
        {
            if (positionsInNiceNumber % 2 != 1)
                throw new ArgumentException("Парамер должен иметь нечетное значение, чтобы была центральное число", nameof(positionsInNiceNumber));

            if (positionsInNiceNumber < 1)
                throw new ArgumentException("Парамер должен иметь нечетное значение >=3", nameof(positionsInNiceNumber));

            Int32 digitPositions = (positionsInNiceNumber - 1) / 2;

            _numbersBuilder = new NumbersBuilder(digitBase, digitPositions);
            _statistic = new Statistic(_numbersBuilder);
            PrintCount = printCount;
        }

        public void Run()
        {
            PrintNiceNumbersCount();
            PrintNiceNumbers();
        }

        private void PrintNiceNumbers()
        {
            // Вывод самих красивых чисел (вы не просили, но мне было интересно доделать)
            // Обращаю внимание: тут используются "ленивые вычисления" - все числа разом не вычисляются, а по необходимости.
            // С одной стороны, это медленнее
            // А с другой стороны, вы можете их получать по реальной потребности - сколько надо, столько и возьмете
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine($"Нажмите любую клавишу для вывода первых {PrintCount} красивых чисел (да, вы это не просили)");
            Console.ReadKey();

            var niceNumberPrinter = new NiceNumberPrinter(_statistic);
            int printed = 0;
            foreach (var niceNumber in niceNumberPrinter.GetNiceNumbers())
            {
                Console.WriteLine(niceNumber);
                if (printed++ >= PrintCount)
                    break;
            }
        }

        private void PrintNiceNumbersCount()
        {
            var niceNumberCounter = new NiceNumberCounter(_statistic);

            // Расчет количество красивых чисел
            var niceNumberCount = niceNumberCounter.EvaluateNiceNumbers();
            Console.WriteLine($"Количество красивых чисел={niceNumberCount}");
        }
    }

}
