﻿using System;
using System.Text;

namespace NiceNumbers
{
    public static class NumberUtil
    {
        /// <summary>
        /// Вычисляет сумму цифр в числе по основанию
        /// </summary>
        /// <param name="number">число</param>
        /// <param name="digitBase">основание</param>
        /// <returns></returns>
        public static byte EvalDigitsSum(int number, int digitBase)
        {
            int sum = 0;

            for (int j = number; j > 0; j = j / digitBase)
            {
                sum += j % digitBase;
            }

            return Convert.ToByte(sum);
        }

        /// <summary>
        /// Целочисленное возведение в степень
        /// </summary>
        /// <param name="num">основание</param>
        /// <param name="pow">степень</param>
        /// <returns></returns>
        public static int IntMathPow(int num, int pow)
        {
            int mul = 1;
            for (int i = 0; i < pow; i++)
            {
                mul *= num;
            }

            return mul;
        }

        /// <summary>
        /// Печатает число в формате по основанию digitBase
        /// </summary>
        /// <param name="number">число</param>
        /// <param name="digitBase">основание</param>
        /// <returns></returns>
        public static void PrintNumber(StringBuilder sb, int number, int digitBase, int positions)
        {
            sb.Clear();

            for (int j = number; j > 0; j = j / digitBase)
            {
                int digit = j % digitBase;
                sb.Insert(0, PrintDigit(digit)); 
            }

            // Вставляем ведущие нули если надо
            int leadZeroes = positions - sb.Length;
            if (leadZeroes > 0)
            {
                for (int i = 0; i < leadZeroes; i++)
                {
                    sb.Insert(0, '0');
                }
            }
        }

        static string constDigits = "0123456789ABCDEFGHIJKLMNOPQR";

        static public char PrintDigit(int digit) => constDigits[digit];
    }
}
