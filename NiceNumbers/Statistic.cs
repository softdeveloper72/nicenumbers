﻿using System;
using System.Collections.Generic;

namespace NiceNumbers
{
    /// <summary>
    /// Класс вычисляет статистику встречаемости числа в массиве
    /// В нашем случае массив это сумм цифр и статистика содержит количество чисел с такой суммой
    /// Примечание: Можно было бы написать на Linq c использованием GroupBy, 
    /// но тогда была бы просадка в скорости, а тут вроде как BigData :)
    /// </summary>
    public class Statistic
    {
        UInt64[] Distribution;

        NumbersBuilder _numbersBuilder;

        public Statistic(NumbersBuilder numbersBuilder)
        {
            _numbersBuilder = numbersBuilder;
            Init();
            EvalStatistic();
        }

        private void Init()
        {
            Distribution = new UInt64[StatisticLen];

            // Это избыточно, но для хорошего тона...
            for (int i = 0; i < Distribution.Length; i++)
            {
                Distribution[i] = 0;
            }
        }

        // Каждая ячейка может хранить число  (ОснованиеСчисления - 1). И еще одно место под НОЛЬ!
        // Например: основание 3, число разрядов - 2. Максимальное число = 22, сумма цифр в разрядах = (3-1)*2=4.
        // Минимальное число=0. Итого, возможно 4+1=5 элементов - 0,1,2,3,4,5
        int StatisticLen => (_numbersBuilder.DigitBase - 1) * _numbersBuilder.Places + 1;
        void EvalStatistic()
        {
            int numbersCount = _numbersBuilder.Count();

            for (int i = 0; i < numbersCount; i++)
            {
                Distribution[Convert.ToInt32(_numbersBuilder[i])]++;
            }
        }

        public IEnumerator<UInt64> GetEnumerator() => ( (IEnumerable<UInt64>) Distribution).GetEnumerator();

        /// <summary>
        ///  Сколько раз число index встречается в массиве
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public UInt64 this[Int32 index] => Distribution[index];
        public int Count() => Distribution.Length;

        public NumbersBuilder GetNumberBuilder() => _numbersBuilder;


    }
}
