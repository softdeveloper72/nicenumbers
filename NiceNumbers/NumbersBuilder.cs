﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NiceNumbers
{
    /// <summary>
    /// Класс вычисляет количество чисел, которые могут содержаться в числе с количеством разрядов places и основанием DigitBase
    /// Порождает массив по этому количеству чисел. 
    /// В массиве каждому числу будет сопоставлено вычисленное сумма цифр (по основанию DigitBase)
    /// Для нашей задачи нам надо 6 позиций с основанием счисления 13. Всего 13^6=4828809 элементов
    /// В каждом разряде может быть максимум 12, разрядов 6, поэтому максимальная сумма цифр в 6 позициях =72.
    /// Для хранения числа нам хватит byte, который может хранить до 255
    /// Таким образом для нашей задачи нам потребуется 5 МБ памяти для хранения массива, что вполне можно себе позволить
    /// </summary>
    public class NumbersBuilder
    {
        byte[] Numbers;     // Для числа по индексу I хранит сумму его цифр по основанию DigitBase

        public int DigitBase { get; }
        public int Places { get;  }

        /// <summary>
        /// Создает массив с предрасчитанными суммами цифр в числе по основанию
        /// </summary>
        /// <param name="digitBase">Основание счисления</param>
        /// <param name="places">Количество разрядов (цифр) в числе</param>
        public NumbersBuilder(int digitBase, int places)
        {
            if (digitBase <= 0) throw new ArgumentOutOfRangeException(nameof(digitBase));
            if (places <= 0) throw new ArgumentOutOfRangeException(nameof(places));

            DigitBase = digitBase;
            Places = places;
            Init();
        }


        void Init()
        {
            int len = NumberUtil.IntMathPow(DigitBase, Places);
            Numbers = new byte[len];

            for (int i = 0; i < Numbers.Length; i++)
            {
                Numbers[i] = NumberUtil.EvalDigitsSum(i, DigitBase);
            }
        }

        public IEnumerator<byte> GetEnumerator() => ((IEnumerable<byte>)Numbers).GetEnumerator();

        public int this [int index]  => Numbers[index];
        public int Count() => Numbers.Count();

        public IEnumerable<Int32> ItemsWithSum(Int32 digitsSum)
        {
            byte byteDigitsSum = (byte)digitsSum;

            for (int i = 0; i < Numbers.Count(); i++)
            {
                if (Numbers[i] == byteDigitsSum)
                {
                    yield return i;
                }
            }
        }
    }
}
