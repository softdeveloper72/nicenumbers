﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace NiceNumbers.Tests
{
    [TestClass()]
    public class NiceNumberPrinterTests
    {
        string[] Results = new string[] { 
            "00000",
            "00100",
            "01001",
            "01101",
            "01010",
            "01110",
            "10001",
            "10101",
            "10010",
            "10110",
            "11011",
            "11111"
        };

        [TestMethod()]
        public void GetNiceNumbersTest()
        {
            int digitBase = 2;
            int digitPositions = 2;

            var numbersBuilder = new NumbersBuilder(digitBase, digitPositions);
            var statistic = new Statistic(numbersBuilder);
            var niceNumberPrinter = new NiceNumberPrinter(statistic);

            var niceNumbers = niceNumberPrinter
                .GetNiceNumbers()
                .OrderBy(x => x)
                .ToArray();

            var sortedResult = Results.OrderBy(x => x).ToArray();

            var isEqual = Enumerable.SequenceEqual(sortedResult, niceNumbers);

            Assert.IsTrue(isEqual);
        }
    }
}