﻿using NiceNumbers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NiceNumbers.Tests
{
    [TestClass()]
    public class NumberUtilTests
    {
        [TestMethod()]
        public void EvalDigitsSumTest()
        {
            // По основанию 10 - обычная система счисления
            Assert.AreEqual(0, NumberUtil.EvalDigitsSum(0, 10));    // 0 => 0
            Assert.AreEqual(1, NumberUtil.EvalDigitsSum(1, 10));    // 1 => 1
            Assert.AreEqual(1, NumberUtil.EvalDigitsSum(10, 10));   // 10 => 1+0=1
            Assert.AreEqual(3, NumberUtil.EvalDigitsSum(12, 10));   // 12 => 1+2=3
        }

        [TestMethod()]
        public void EvalDigitsSumBinaryTest()
        {
            // По основанию 2! Двоичная система счисления
            Assert.AreEqual(0, NumberUtil.EvalDigitsSum(0, 2));
            Assert.AreEqual(1, NumberUtil.EvalDigitsSum(1, 2)); // 01 => 0+1=1
            Assert.AreEqual(1, NumberUtil.EvalDigitsSum(2, 2)); // 10 => 1+0=1
            Assert.AreEqual(2, NumberUtil.EvalDigitsSum(3, 2)); // 11 => 1+1=2
        }

        [TestMethod()]
        public void IntMathPowTest()
        {
            Assert.AreEqual(1, NumberUtil.IntMathPow(0, 0));    // Любое число в 0 степени =1
            Assert.AreEqual(2, NumberUtil.IntMathPow(2, 1));    // 2^1=2
            Assert.AreEqual(8, NumberUtil.IntMathPow(2, 3));    // 2*2*2=8
        }

        [TestMethod()]
        public void PrintDigitTest()
        {
            Assert.AreEqual('0', NumberUtil.PrintDigit(0));
            Assert.AreEqual('A', NumberUtil.PrintDigit(10));
            Assert.AreEqual('C', NumberUtil.PrintDigit(12));
        }
    }
}