﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NiceNumbers.Tests
{
    [TestClass()]
    public class NiceNumberCounterTests
    {
        [TestMethod()]
        public void EvaluateNiceNumbersBase2Digits1Test()
        {
            int digitBase = 2;
            int digitPositions = 1;

            // центральная цифра 0
            // 2 комбинации
            // 0 0 0
            // 1 0 1

            // центральная цифра 1
            // 2 комбинации
            // 0 1 0
            // 1 1 1

            // Итого: 4 красивых числа
            var numbersBuilder = new NumbersBuilder(digitBase, digitPositions);
            var statistic = new Statistic(numbersBuilder);

            var niceNumberCounter = new NiceNumberCounter(statistic);

            var niceNumberCount = niceNumberCounter.EvaluateNiceNumbers();
            Assert.AreEqual((UInt64)4, niceNumberCount);
        }

        [TestMethod()]
        public void EvaluateNiceNumbersBase1Digits2Test()
        {
            int digitBase = 3;
            int digitPositions = 2;


            // центральная цифра 0
            // 2 комбинации
            // 0 0  => 0  
            // 0 1  => 1    
            // 0 2  => 2  
            // 1 0  => 1  
            // 1 1  => 2    
            // 1 2  => 3    
            // 2 0  => 2    
            // 2 1  => 3    
            // 2 2  => 4    

            // 0 - 1 шт => 1 комб
            // 1 - 2 шт => 4 комб
            // 2 - 3 шт => 9 комб
            // 3 - 2 шт => 4 комб
            // 4 - 1 шт => 1 комб
            // -----------------------
            // 19 комбинаций на 1 центральнаю цифру

            // Центральных цифры 3 => 3*19 = 57 красивых чисел
            var numbersBuilder = new NumbersBuilder(digitBase, digitPositions);
            var statistic = new Statistic(numbersBuilder);

            var niceNumberCounter = new NiceNumberCounter(statistic);

            var niceNumberCount = niceNumberCounter.EvaluateNiceNumbers();
            Assert.AreEqual((UInt64)57, niceNumberCount);
        }
    }
}