﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NiceNumbers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiceNumbers.Tests
{
    [TestClass()]
    public class NumbersBuilderTests
    {
        [TestMethod()]
        public void NumbersBuilderTest()
        {
            // 2 разряда по основанию 3.
            var numbersBuilder = new NumbersBuilder(3, 2);
            
            int i = 0;
            Assert.AreEqual(0, numbersBuilder[i++]);    // 00
            Assert.AreEqual(1, numbersBuilder[i++]);    // 01
            Assert.AreEqual(2, numbersBuilder[i++]);    // 02
            Assert.AreEqual(1, numbersBuilder[i++]);    // 10
            Assert.AreEqual(2, numbersBuilder[i++]);    // 11
            Assert.AreEqual(3, numbersBuilder[i++]);    // 12
            Assert.AreEqual(2, numbersBuilder[i++]);    // 20
            Assert.AreEqual(3, numbersBuilder[i++]);    // 21
            Assert.AreEqual(4, numbersBuilder[i++]);    // 22
        }

        //[TestMethod()]
        //public void NumbersBuilderPrint()
        //{
        //    // 2 разряда по основанию 3.
        //    var numbersBuilder = new NumbersBuilder(2, 2);

        //    var result = new List<Int32>();

        //    for (int i = 0; i < numbersBuilder.Count(); i++)
        //    {
        //        if (numbersBuilder[i] == 3)
        //        {
        //            result.Add(i);
        //        }
        //    }

        //    var arrResult = result.ToArray();
        //}
    }
}