﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NiceNumbers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiceNumbers.Tests
{
    [TestClass()]
    public class StatisticTests
    {
        [TestMethod()]
        public void StatisticTest()
        {
            int digitBase = 13;
            int digitPositions = 6;


            // Итого: 4 красивых числа
            var numbersBuilder = new NumbersBuilder(digitBase, digitPositions);
            var statistic = new Statistic(numbersBuilder);

            UInt64 sum = 0;
            for (int i = 0; i < statistic.Count(); i++)
            {
                sum += statistic[i];
            }

            Assert.AreEqual(sum, (UInt64)numbersBuilder.Count());
        }

        [TestMethod()]
        public void StatisticTest1()
        {
            int digitBase = 6;  // максимум 5 шаров в каждый ящик. Значит, не 6-ричная система
            int digitPositions = 6; // 6 ящиков


            // Итого: 4 красивых числа
            var numbersBuilder = new NumbersBuilder(digitBase, digitPositions);
            var statistic = new Statistic(numbersBuilder);

            UInt64 sum = 0;
            for (int i = 0; i < statistic.Count(); i++)
            {
                sum += statistic[i];
            }

            Assert.AreEqual(sum, (UInt64)numbersBuilder.Count());
        }
    }
}